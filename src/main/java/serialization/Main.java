package serialization;

import serialization.io.Serialization;
import serialization.model.Droid;
import serialization.model.Ship;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Droid> droids = new ArrayList<>();
        droids.add(new Droid("S549DS", 200, 600, 100));
        droids.add(new Droid("A48SEL", 250, 800, 100));

        Ship ship = new Ship(droids);

        Serialization.serialize(ship, "droids.dat");

        Ship receivedShip = Serialization.deserialize("droids.dat");
        System.out.println(receivedShip);

    }
}
